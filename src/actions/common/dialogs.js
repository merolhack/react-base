/**
 * @function Update Login error Text
 * @param string text
 * @return {object} 
 */
export function updateLoginErrorText(text) {
    return {
        type: 'UPDATE_DIALOGS_LOGIN_ERROR_TEXT',
        payload: text
    };
}
/**
 * @function Update Login error Open
 * @param boolean open
 * @return {object} 
 */
export function updateLoginErrorOpen(open) {
    return {
        type: 'UPDATE_DIALOGS_LOGIN_ERROR_OPEN',
        payload: open
    };
}
/**
 * @function Update User Welcome Open
 * @param boolean open
 * @return {object} 
 */
export function updateUserWelcomeOpen(open) {
    return {
        type: 'UPDATE_DIALOGS_USER_WELCOME_OPEN',
        payload: open
    };
}
/**
 * @function Update Loading Open
 * @param boolean open
 * @return {object} 
 */
export function updateLoadingOpen(open) {
    return {
        type: 'UPDATE_DIALOGS_LOADING_OPEN',
        payload: open
    };
}
/**
 * @function Update Session Timer Open
 * @param boolean open
 * @return {object} 
 */
export function updateSessionTimerOpen(open) {
    return {
        type: 'UPDATE_DIALOGS_SESSION_TIMER_OPEN',
        payload: open
    };
}
/**
 * @function Update Select Company Alert
 * @param boolean open
 * @return {object} 
 */
export function updateSelectCompanyOpen(open) {
    return {
        type: 'UPDATE_DIALOGS_SELECT_COMPANY_OPEN',
        payload: open
    };
}
/**
 * @function 
 * @return {object} 
 */
export function resetState() {
    return {
        type: 'RESET_DIALOGS'
    };
}
