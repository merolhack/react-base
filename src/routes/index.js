/**
 * 
 */
import React from 'react';
import { Router, Route, IndexRedirect } from 'react-router';
/** Main container */
import Layout from 'containers/Layout';
/** Auth function container */
import { requireAuthentication } from 'containers/AuthenticatedComponent';
/** Pages Components */
import * as Pages from 'components/base/pages';
/** Forms Components */
import * as Forms from 'components/base/forms';
/** Legacy Systems Components */
import * as LegacySystems from 'components/legacySystems';
/**
 * @desc Application Routes
 */
const routes = (
	<Router>
		<Route path="/" component={Layout}>
			<IndexRedirect to="home" />
			<Route path="login" component={Pages.Login} />
			<Route path="home" component={requireAuthentication(Pages.Home)} />
			<Route path="configuration" component={requireAuthentication(Forms.Configuration)} />
			<Route path="gap/:menuKey" component={requireAuthentication(LegacySystems.GAP)} />
			<Route path="buro/:menuKey" component={requireAuthentication(LegacySystems.BURO)} />
			<Route path="sic/:menuKey" component={requireAuthentication(LegacySystems.SIC)} />	
			<Route path="sif/:menuKey" component={requireAuthentication(LegacySystems.SIF)} />	
			<Route path="*" component={requireAuthentication(Pages.NotFoundPage)} />
		</Route>
	</Router>
);
export default routes;
