let IP = '';
let PORT = '';
// Only the development enviroment
switch(process.env.NODE_ENV) {
    case 'production':
    case 'testing':
        IP = '10.1.44.86';
        PORT = '8182';
    break;
    case 'development':
        // Desarrollo
        IP = '10.1.44.86';
        PORT = '8182';
        // Arturo
        // IP = '192.168.252.217';
        // PORT = '8181';
        // Antonio
        // IP = '192.168.253.215';
        // PORT = '8181';
    break;
}
/** Login: Employees */
export const URI_LOGIN_EMPLOYEES = 'http://'+IP+':'+PORT+'/cxf/empleados/rest';// Antonio
/** Login: Employees > Find Active */
export const URI_LOGIN_EMPLOYEES_SEARCH_ACTIVE = URI_LOGIN_EMPLOYEES + '/buscarActivos';
/** Login: Security */
export const URI_LOGIN_SECURITY = 'http://'+IP+':'+PORT+'/cxf/seguridad';
// Antonio
/** Login: Security > Consult Users */
export const URI_LOGIN_SECURITY_CONSULT_USER_EMAIL = URI_LOGIN_SECURITY + '/autorizacion/consultarUsuarioEmail';
export const URI_LOGIN_SECURITY_CONSULT_USERS = URI_LOGIN_SECURITY + '/autorizacion/consultarUsuarios';
/** Login: Security > Authorization > Record Access Log */
export const URI_LOGIN_SECURITY_AUTHORIZATION_RECORD_ACCESS_LOG = URI_LOGIN_SECURITY + '/autorizacion/registrarBitacoraAcceso';
/** Login: Security > Authorization > Get Access Log */
export const URI_LOGIN_SECURITY_AUTHORIZATION_GET_ACCESS_LOG = URI_LOGIN_SECURITY + '/autorizacion/consultarBitacoraAcceso';
/** Menu: Consult */
export const URI_MENU_CONSULT = URI_LOGIN_SECURITY + '/autorizacion/consultarMenus';
