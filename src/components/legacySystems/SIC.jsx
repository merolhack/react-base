/**
 * 
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'underscore';
/** Redux actions */
import * as IFrameActions from 'actions/legacySystems/iframe';
import * as DialogsActions from 'actions/common/dialogs';

@connect((store) => {
    return {
        userInfo: store.commonAuth.userInfo,
        menuData: store.commonMenu.data,
        iframeUri: store.legacySystemsIframe.uri,
        sicIsLoaded: store.legacySystemsIframe.sicIsLoaded,
        menuKey: store.legacySystemsIframe.menuKey,
        companySIC: store.commonConfiguration.companySIC
    };
})
/**
 * @desc SIC: Legacy System Component
 */
class SIC extends Component {
    constructor (props) {
        super(props);
        // Check if the iFrame exists on the DOM
        this.exists = 0;
    }
    componentWillMount() {
        this.props.dispatch(IFrameActions.updateMenuKey(this.props.routeParams.menuKey));
        if ( this.props.companySIC === null ) {
            this.props.dispatch(DialogsActions.updateSelectCompanyOpen(true));
        } else {
            this._getURI();
            this.exists = window.$('iframe#SICiFrame-'+this.props.routeParams.menuKey+'-'+this.props.companySIC).length;
            if ( this.exists !== 0 ) {
                window.$('iframe#SICiFrame-'+this.props.routeParams.menuKey+'-'+this.props.companySIC).show();
            }
        }
    }
    /**
     * @desc: Get the URL for the iFrame
     */
    _getURI = () => {
        let menuKey = (!_.isUndefined(this.props.routeParams)) ? this.props.routeParams.menuKey : null ;
        let user = (!_.isUndefined(this.props.userInfo)) ? this.props.userInfo.usuario : null ;
        this.props.dispatch(IFrameActions.updateIFrameURI(menuKey, this.props.menuData, user, this.props.companySIC));
        this.props.dispatch(DialogsActions.updateLoadingOpen(true));
    }
    render() {
        if ( this.props.companySIC !== null ) {
            // If the user select another menu item
            // Update the menuKey and get again the URL
            if ( this.props.menuKey !== this.props.routeParams.menuKey ) {
                this.props.dispatch(IFrameActions.updateMenuKey(this.props.routeParams.menuKey));
            }
            this.exists = window.$('iframe#SICiFrame-'+this.props.routeParams.menuKey+'-'+this.props.companySIC).length;
            if ( this.props.iframeUri !== null && this.exists === 0 ) {
                window.$(document).ready(() => {
                    if ( window.$('iframe#SICiFrame-'+this.props.routeParams.menuKey).length === 0 ) {
                        window.$('#component-wrapper').append('<iframe id="SICiFrame-'+this.props.routeParams.menuKey+'-'+this.props.companySIC+'" name="SICiFrame" frameBorder="0" width="100%" height="800">Your browser does not support inline frames.</iframe>');
                        window.$('iframe#SICiFrame-'+this.props.routeParams.menuKey+'-'+this.props.companySIC).attr('src', this.props.iframeUri);
                        window.$('iframe#SICiFrame-'+this.props.routeParams.menuKey+'-'+this.props.companySIC).on('load', () => {            
                            this.props.dispatch(DialogsActions.updateLoadingOpen(false));
                            this.props.dispatch(IFrameActions.updateSICIsLoaded(true));
                        });
                    }
                });
            }
            // Check if the SIC is already loaded and the URIs are the same
            if ( this.props.sicIsLoaded === true ) {
                let currentIFrameURI = window.$('iframe#SICiFrame-'+this.props.routeParams.menuKey+'-'+this.props.companySIC).attr('src');
                if ( currentIFrameURI !== this.props.iframeUri ) {
                    window.$('iframe#SICiFrame-'+this.props.routeParams.menuKey+'-'+this.props.companySIC).attr('src', this.props.iframeUri);
                    window.$('iframe#SICiFrame-'+this.props.routeParams.menuKey+'-'+this.props.companySIC).on('load', () => {
                        this.props.dispatch(DialogsActions.updateLoadingOpen(false));
                    });
                }
            }
        }
        return (
            <div></div>
        );
    }
    componentWillUnmount() {
        if ( this.props.companySIC !== null ) {
            window.$('iframe#SICiFrame-'+this.props.routeParams.menuKey+'-'+this.props.companySIC).hide();
            this.props.dispatch(IFrameActions.resetIFrameURI());
        }
    }
}
export default SIC;
