/**
 * Common pages
 */
export {default as Login} from './Login';
export {default as Home} from './Home';

// 404 Component
export {default as NotFoundPage} from './NotFoundPage';
