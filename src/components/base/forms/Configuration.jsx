/**
 * 
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
/** Material-UI */
import Paper from 'material-ui/Paper';
import Snackbar from 'material-ui/Snackbar';
import RaisedButton from 'material-ui/RaisedButton';
import {Card, CardHeader, CardText} from 'material-ui/Card';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
/** Redux actions */
import * as LayoutActions from 'actions/common/layout';
import * as ConfigurationActions from 'actions/common/configuration';
/** Custom styles */
const style = {
    margin: 12,
};
@connect((store) => {
    return {
    };
})
/**
 * 
 */
class Configuration extends Component {
    constructor(props) {
        super(props);
        // 
        this.state = {
            open: false
        };
        // Bind methods
        this._handleSaveOnClik = this._handleSaveOnClik.bind(this);
        this._handleRequestClose = this._handleRequestClose.bind(this);
        this._handleResetOnClik = this._handleResetOnClik.bind(this);
        this._handleBlueOnClik = this._handleBlueOnClik.bind(this);
    }
    _handleSaveOnClik() {
        this.setState({
            open: true,
        });
    }
    _handleRequestClose() {
        this.setState({
            open: false,
        });
    }
    _handleResetOnClik() {
        this.props.dispatch(LayoutActions.resetHeaderBackgroundColor());
    }
    _handleBlueOnClik() {
        this.props.dispatch(LayoutActions.updateHeaderBackgroundColor('#0000FF'));
    }
    render() {
        return (
            <Paper>
                <div className='row'>
                    <div className='col m10'>
                        <h4>Configuración</h4>
                    </div>
                    <div className='col m2'>
                        <RaisedButton 
                            label="Guardar" 
                            primary={true} 
                            style={style}
                            onTouchTap={this._handleSaveOnClik} />
                    </div>
                </div>
                <div className='row'>
                    <div className='col m6'>
                        <Card>
                            <CardHeader
                            title="Tema"
                            subtitle="Color y logo de la aplicación"
                            actAsExpander={true}
                            showExpandableButton={true}
                            />
                            <CardText expandable={true}>
                                <div className='row'>
                                    <div className='col m6'>
                                        <RaisedButton 
                                            label="Financiera Independencia" 
                                            secondary={true} 
                                            style={style}
                                            onTouchTap={this._handleResetOnClik} />
                                    </div>
                                    <div className='col m6'>
                                        <RaisedButton 
                                            label="Mas Nomina" 
                                            primary={true} 
                                            style={style}
                                            onTouchTap={this._handleBlueOnClik} />
                                    </div>
                                </div>
                            </CardText>
                        </Card>
                    </div>
                    <div className='col m6'>
                    </div>
                </div>
                <Snackbar
                    open={this.state.open}
                    message="Se ha guardado la información"
                    autoHideDuration={4000}
                    onRequestClose={this._handleRequestClose}
                    />
            </Paper>
        );
    }
}
export default Configuration;
