/**
 * 
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import FontAwesome from 'react-fontawesome';
import _ from 'underscore';
/** Material-UI */
import Drawer from 'material-ui/Drawer';
import Divider from 'material-ui/Divider';
import ListItem from 'material-ui//List/ListItem';
import ActionFaceIcon from 'material-ui/svg-icons/action/face';
import AlertWarningIcon from 'material-ui/svg-icons/alert/warning';
/** Redux actions */
import { updateMenuOpen } from 'actions/common/menu';
/** Images */
//import LogoImg from 'styles/images/layout/logo-menu.png';
import LogoImg from 'styles/images/layout/horizontal-logo.png';
/** Inline Styles to override the Material-UI Styles */
const styles = {
    menuContainer: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        height: '100%',
    },
    menu: {
        listItems: {
            'height': '100%',
            'minHeight': '100%',
            'position': 'absolute',
            'top': 0,
            'width': '100%'
        },
        bottomLinks: {
            'position': 'absolute',
            'bottom': 0
        }
    },
    drawer: {
        Root: {
            Opened: {
                color: 'rgba(0, 0, 0, 0.870588)',
                backgroundColor: 'rgb(255, 255, 255)',
                transition: 'margin 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
                boxSizing: 'border-box',
                boxShadow: 'rgba(0, 0, 0, 0.117647) 0px 1px 6px, rgba(0, 0, 0, 0.117647) 0px 1px 4px',
                borderRadius: '2px',
                flex: '0 0 16em',
                marginLeft: '0px',
                order: '-1'
            },
            Closed: {
                flex: '0 0 16em',
                marginLeft: '-16em',
                order: '-1'
            }
        },
        Container: {
            Open: {
                borderRadius: '0',
                boxShadow: 'none', 
                position: 'static',
                height: 'auto',
                width: 'auto',
                float: 'none',
                top: 'auto',
                left: 'auto'
            }
        }
    }
};
/** 
 * Hardcoded
    const itemMenuIcons = {
        'mnu_gap': require('styles/images/menu/icon_ap.jpg'),
        'mnu_buro': require('styles/images/menu/icon_burocredito.png'),
        'mnu_sic': require('styles/images/menu/icon_sic.jpg'),
        'mnu_sif': require('styles/images/menu/icon_sif.jpg')
    };
 */
/** Redux state */
@connect((store) => {
    return {
        menuOpen: store.commonMenu.open,
        menuData: store.commonMenu.data
    };
})
/**
 * @desc MenuLeft Class Component
 */
class MenuLeft extends Component {
    /**
     * 
     */
    constructor (props) {
        super(props);
    }
    /**
     * 
     */
    childItems = (parentIndex, parentKey) => {
        let array = [];
        this.props.menuData.map((item, child) => {
            if ( item.codigoPadre === null ) {
                item.menus.map((childItem, child) => {
                    if ( childItem !== null && childItem.codigoPadre == parentKey ) {
                        array.push(
                            <ListItem key={childItem.codigoMenu}
                                primaryText={
                                    <Link
                                    onClick={this.props.close}
                                    id={childItem.codigoMenu}
                                    to={{ pathname: childItem.codigoModulo + '/' + childItem.codigoMenu}}
                                    key={childItem.codigoMenu}>{childItem.nombre}</Link>
                                }/>);
                    }
                });
            }
        });
        return array;
    }
    /**
     * 
     */
    _handleOnClickNewWindow( type ) {
        let url = null;
        switch( type ) {
            case 'eureka':

            break;
            case 'service_desk':

            break;
        }

    }
    /**
     * 
     */
    render() {
        let Menuitems = this.props.menuData.map((menu, index) => {
            //let leftIcon = (menu.codigoMenu !== 'mnu_buro') ? <img src={itemMenuIcons[menu.codigoMenu]} /> : itemMenuIcons[menu.codigoMenu] ;
            let icon_path = 'assets/images/default.png';
            if (  menu.icono !== null ) {
                icon_path = 'assets/images/' + menu.icono;
            }
            let leftIcon = <img src={icon_path} /> ;
            // Get only the parent items
            return(
                <ListItem 
                    key={menu.codigoMenu}
                    primaryText={menu.nombre}
                    leftIcon={leftIcon}
                    primaryTogglesNestedList={true}
                    nestedItems={this.childItems(index, menu.codigoMenu)}/>
            );
        });
        let rootStyle = ( this.props.menuOpen ) ? styles.drawer.Root.Opened : styles.drawer.Root.Closed ;
        return (
            <Drawer 
                open={this.props.menuOpen}
                docked={false}
                style={rootStyle}
                
                onRequestChange={() => this.props.dispatch(updateMenuOpen(!this.props.menuOpen))}>
                    {/*containerStyle={styles.drawer.Container.Open}*/}
                    <div style={styles.menu.listItems}>{/* style={styles.menuContainer}*/}
                        <center>
                            <p>
                                <img src={LogoImg} />
                            </p>
                        </center>
                        <Divider />
                        {Menuitems}
                        <Divider />
                        <a href='http://eureka/' title='EUREKA' target='_blank'>
                            <ListItem 
                                primaryText={'Eureka'}
                                leftIcon={<ActionFaceIcon />}
                                />
                        </a>
                        <a href='http://172.30.7.9/MRcgi/MRentrancePage.pl' title='SERVICE DESK' target='_blank'>
                            <ListItem 
                                primaryText={'Service Desk'}
                                leftIcon={<AlertWarningIcon />}
                                />
                        </a>
                        <div className='row' style={styles.menu.bottomLinks}>
                            <div className='col m12'>
                                <div className='row menu-links'>
                                    <div className='col m4 offset-m2'>
                                        <a href='http://www.independencia.com.mx/terminos_y_condiciones/terminos_y_condiciones_de_uso_y_privacidad/' target='_blank'>Términos</a>
                                    </div>
                                    <div className='col m1'>
                                        <span className='center-align'>&nbsp;|</span>
                                    </div>
                                    <div className='col m4'>
                                        <a href='#'>Contacto</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </Drawer>
        );
    }
}
MenuLeft.propTypes = {
    close: PropTypes.function
};
export default MenuLeft;
