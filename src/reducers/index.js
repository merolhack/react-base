/**
 * 
 */
import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
/**  */
import commonAuth from './common/auth';
import commonDialogs from './common/dialogs';
import commonMenu from './common/menu';
import commonGapi from './common/gapi';
/**  */
import commonLayout from './common/layout';
/** User Configuration */
import commonConfiguration from './common/configuration';
/**  */
import legacySystemsIframe from './legacySystems/iframe';

/**
 * 
 */
export default combineReducers({
    commonAuth,
    commonDialogs,
    commonMenu,
    commonGapi,
    commonLayout,
    commonConfiguration,
    legacySystemsIframe,
    routing: routerReducer
});
