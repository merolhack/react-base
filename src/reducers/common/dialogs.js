/**
 * Authentification
 */
import * as DialogsConstants from 'constants/dialogs';

const initialState = {
    // Error dialog properties
    loginErrorTitle: DialogsConstants.DIALOGS_LOGIN_ERROR_TITLE,
    loginErrorIcon: true,
    loginErrorText: null,
    loginErrorButtonText: null,
    loginErrorOpen: false,
    // User welcome open/close
    userWelcome: false,
    // Loading open/close
    loading: false,
    // Loading open/close
    sessionTimer: false,
    // Select company warning
    selectCompany: false,
};
/**
 * 
 */
export default function reducer(state=initialState, action) {
    switch(action.type) {
    case 'UPDATE_DIALOGS_LOGIN_ERROR_TITLE' : {
        return {...state, loginErrorTitle: action.payload};
    }
    case 'UPDATE_DIALOGS_LOGIN_ERROR_ICON' : {
        return {...state, loginErrorIcon: action.payload};
    }
    case 'UPDATE_DIALOGS_LOGIN_ERROR_TEXT' : {
        return {...state, loginErrorText: action.payload};
    }
    case 'UPDATE_DIALOGS_LOGIN_ERROR_BUTTON_TEXT' : {
        return {...state, loginErrorButtonText: action.payload};
    }
    case 'UPDATE_DIALOGS_LOGIN_ERROR_OPEN' : {
        return {...state, loginErrorOpen: action.payload};
    }
    case 'UPDATE_DIALOGS_USER_WELCOME_OPEN' : {
        return {...state, userWelcome: action.payload};
    }
    case 'UPDATE_DIALOGS_LOADING_OPEN' : {
        return {...state, loading: action.payload};
    }
    case 'UPDATE_DIALOGS_SESSION_TIMER_OPEN' : {
        return {...state, sessionTimer: action.payload};
    }
    case 'UPDATE_DIALOGS_SELECT_COMPANY_OPEN' : {
        return {...state, selectCompany: action.payload};
    }
    case 'RESET_DIALOGS' : {
        return {...state, 
            loginErrorTitle: initialState.loginErrorTitle,
            loginErrorIcon: initialState.loginErrorIcon,
            loginErrorText: initialState.loginErrorText,
            loginErrorButtonText: initialState.loginErrorButtonText,
            loginErrorOpen: initialState.loginErrorOpen,
            userWelcome: initialState.userWelcome,
            loading: initialState.loading,
            sessionTimer: initialState.sessionTimer,
            selectCompany: initialState.selectCompany,
        };
    }
    }
    return state;        
}
