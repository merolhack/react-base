/**
 * 
 */

/**
 * 
 */
const initialState = {
    data: [],
    open: false
};

/**
 * @desc Menu reducer
 */
export default function reducer(state=initialState, action) {
    switch(action.type) {
    case 'UPDATE_MENU_DATA' : {
        return {...state, 
            data: action.payload,
        };
    }
    case 'UPDATE_MENU_OPEN' : {
        return {...state, 
            open: action.payload,
        };
    }
    case 'RESET_MENU' : {
        return {...state, 
            data: initialState.data,
            open: initialState.open
        };
    }
    }
    return state;        
}
